<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sRK,ksg~J^m)q5)<saa%2+k#BE:K${mM,BL`N<13&wCLT6FUI/ps]G48b5_~DL:,' );
define( 'SECURE_AUTH_KEY',  ',&*.~+/PFaf|1^>OlB5!Uk<,ouCF|SP),<{+zP=gVny9.95GZvP7X{ETd+Yp8x|e' );
define( 'LOGGED_IN_KEY',    '-A NSc(pj?M2U@5_UgY 5sLd?]]l|24sH&V~t`r!-_l8]OJs3yC%rq$*@W4j^kHw' );
define( 'NONCE_KEY',        '-`dDocGtVPGB3ZkhF27}2_8l2(,KUbU8@IbMkQn+0?-3QE;+u+c]M6F*[JH{V<qH' );
define( 'AUTH_SALT',        'HzI;in=Ww]s-1@i&#|hA5H{uI5&<U%{akcbxo?>-fc&VixV<P2G;A)yqaz0 lwx-' );
define( 'SECURE_AUTH_SALT', 'y:PF`LLIM5nT~v8|0YZT~YyL=$sRQvjPxo $!tup0@Z1$8fP5He&}~V#3nnWFH@g' );
define( 'LOGGED_IN_SALT',   '?BDHudpp]q=e7H8 %fq^A#@eIig_Leh =aT`o]@xIxw-h~d5Rj7f9pp`Rjq<Hl97' );
define( 'NONCE_SALT',       'OgQCU >yu^-}M[P`~,-vrtJ7 agmk$&w(}j,pY-TQLz88l|/iUFPjlr`wu,+fS}E' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
